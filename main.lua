--[[

This is an extremely basic sample playdate game.  
There are many features of the Playdate SDK
that are not used in this example.

Read the API reference here: 
https://sdk.play.date/1.9.3/Inside%20Playdate.html

]]--

--shortcuts to frequently used functions
pg = playdate.graphics
width, height = playdate.display.getSize()


--define a function to setup some things that will load one time, when the game starts
--this function can be named anything you want
function load()
  
  playdate.startAccelerometer() --accelerometer is off by default, so turn it on!
  
  --create player object and an enemy object
  player = {x = width/2, y = height-32, img = pg.image.new("player")}
  enemy = {x = math.random(width), y = -32, img = pg.image.new("enemy")}
  
end

--call the load() function we just defined one time, at the start
load()


--update() runs every frame,
--this is where you put movement code, animation code, or physics
--you will draw stuff in this function as well, there is not a separate draw callback function
function playdate.update()
  
  --reseed math.random() every frame
  math.randomseed(playdate.getSecondsSinceEpoch())
  
  --clear the screen every frame
  pg.clear()

  --the enemy starts at the top of the screen and moves down,
  --when the enemy reaches the bottom of the screen, move it back to the top
  enemy.y = enemy.y + 5
  if enemy.y > height then
    enemy.x,enemy.y = math.random(width),-32
  end

  --if the enemy hits the player, end the game
  --see the hit function at line 82
  if hit(player.x,player.y,32,32,enemy.x,enemy.y,32,32) then
    pg.drawText("GAME OVER!",width/2,height/2)
    playdate.stop() --this just stops the game loop entirely
  end

  --move the player by d-pad
  if playdate.buttonIsPressed("left") then
      player.x = player.x - 5
  elseif playdate.buttonIsPressed("right") then
      player.x = player.x + 5
  end
  
  --move the player by accelerometer
  local ax,ay,az = playdate.readAccelerometer()
  player.x = player.x + ax*5
  
  --draw the player and the enemy
  player.img:draw(player.x,player.y)
  enemy.img:draw(enemy.x,enemy.y)
  
end

--move player by crank
function playdate.cranked(change)
  
  player.x = player.x + change --change is just a number value for 'degrees'
  
end

--aabb collision (two rectangular collision boxes)
--returns true if two rects are overlapping
function hit(x1,y1,w1,h1,x2,y2,w2,h2)
  
  return x1 < x2+w2 and
  x1+w1 > x2 and
  y1 < y2+h2 and
  y1+h1 > y2
  
end
